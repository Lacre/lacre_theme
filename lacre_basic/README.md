# Base Theme Theme

The **Base Theme** Theme is for [Lacre.io](https://git.disroot.org/Disroot/gpg-lacre).  This README.md file should be modified to describe the features, installation, configuration, and general usage of this theme.

## Description

Base theme to build upon
